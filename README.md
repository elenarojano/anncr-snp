# Anncrsnp

AnNCR-SNP integrates data from various sources, allowing the user to obtain annotation to investigate the potential effects of variation in non-coding regions of the human genome. AnNCR-SNP consists of a database containing data on all non-coding elements and two main programs: manager and finder. The manager program is responsible for creating a local database in the user's computer', and the finder program queries the local database, returning a table of results. Local database is already built and it is downloaded when the finder program is used for the first time. If the user wants to build the local database with custom information, he/she has to use the manager program.  

The user can mine the local database, searching information about SNPs that overlap with various genomic features suggestive of regulatory activity, such as TFBs, open chromatin, histone modifications, methylation sites and enhancers. These genomic features were obtained from a number of different projects and data sources (ENCODE, FANTOM5, DENdb, amongst others). SNP information comes from dbSNP, gene information from RefSeq and conserved regions from 46WayCons.

If you use this tool, please cite us: Rojano E, Ranea JA, Perkins JR. Characterisation of non-coding genetic variation in histamine receptors using AnNCR-SNP. Amino Acids. 2016 Jun 6. DOI: 10.1007/s00726-016-2265-5.

## Installation
### Linux (Ubuntu) & Mac

If you have no Ruby installed, we recommend you to install Ruby using RVM:

    $ gpg2 --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3

If this command doesn't work, try:

    $ command curl -sSL https://rvm.io/mpapis.asc | gpg2 --import -

Then type:

    $ \curl -sSL https://get.rvm.io | bash -s stable
    $ source /home/user/.profile

Check that the file ~/.bashrc contains this line at the end, if not ruby won't be recognized:
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

Then execute:
    
    $ rvm install 2.1.1

At this point, you can install the ruby gem:

    $ gem install anncrsnp

For further RVM installation, please visit https://rvm.io/

### Windows

To use AnNCR-SNP in your Windows system, you can install Ruby using RubyInstaller (http://rubyinstaller.org/downloads/), but taking into consideration that Ruby version must be lower than 2.3 and x64. You can download this concrete version: http://dl.bintray.com/oneclick/rubyinstaller/rubyinstaller-2.2.5-x64.exe

During the installation, you have to mark all available options before continuing (Install Td/Tk support, Add Ruby executables to your PATH, Associate .rb and .rbw files with this Ruby installation).

Then, install the package directly as:

    $ gem install anncrsnp

For further RubyInstaller information, please visit http://rubyinstaller.org/



Install the package directly as:

    $ gem install anncrsnp

## Usage

### Finder

The user can query the local database using a list of SNPs or genomic coordinates. When the user runs the first query using the finder program (grdbfinder.rb), AnNCR-SNP will download the database (it is downloaded by default in the same directory where the ruby gem is installed). Then, it will accomplish the data search.

An example of use can be the following:

    $ grdbfinder.rb -n rs2470893,rs12049351 -g snpDbSnp -F 200 -o output_file -f txt

Where:

```
-n: SNP identifier(s) to be queried. The user can also give gene identifiers (RefSeq gene symbols), or use the -c command instead of -n for search coordinates in the following format: chr:start:stop (example: chr3:11128779:11178779).
-g: when is set with 'snpDbSnp', the script generates a tabular file with each found SNP that overlaps with some feature of interest (regulatory element, gene, etc).
-F: flanking region length (in nucleotides) located up and downstream for each SNP, gene or coordinate queried. Used for increasing the range of search.
-o: output file name.
-f: output file format. Supported formats: .txt, .html.
```

Optional flags:

```
-r: for a graphical representation. Format .gff3
-p: path to a custom database. Use in case of the database is created by the user with the manager program, and the user doesn't want to use the default database.
```

Note: if this is the first query you perform, the program will download the database. It can take a time depending on your Internet connection. Database size: 1.5GB.

The user can also give to AnNCR-SNP finder a file with coordinates (use flag -c) or a list of SNPs or genes for searching (use flac -n). File must contain each element separated by line breaks.

### Manager

Only used if the user wants to build a local database with custom information. - In construction -.

## Contributing

Bug reports and pull requests are welcome. Please contact with the ruby gem anncrsnp developer (elenarojano at uma.es).


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

