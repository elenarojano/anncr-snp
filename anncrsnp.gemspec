# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'anncrsnp/version'

Gem::Specification.new do |spec|
  spec.name          = "anncrsnp"
  spec.version       = Anncrsnp::VERSION
  spec.authors       = ["Elena Rojano", "Pedro Seoane"]
  spec.email         = ["elenarojano@outlook.com", "seoanezonjic@hotmail.com"]

  spec.summary       = %q{Tool to characterize Single Nucleotide Polymorphisms (SNP) in genomic non-coding regions.}
  spec.description   = %q{AnNCR-SNP integrates data from various sources, allowing the user to investigate the potential effects of variants in non-coding regions of the human genome. AnNCR-SNP consists of a database containing data on all non-coding elements and two main programs: manager and finder. The manager program is responsible for creating the local data-base, and the finder program receives the user queries in order to search in the local database and retrieve information. The user can find information about various regu-latory elements, such as TFBs, open chromatin, histone modification and methyla-tion sites, information about SNPs from dbSNP and gene information from RefSeq.}
  spec.homepage      = ""
  spec.license       = "MIT"

  # # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # # delete this section to allow pushing this gem to any host.
  # if spec.respond_to?(:metadata)
  #   spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  # else
  #   raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  # end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"
  spec.add_dependency "sqlite3"
  spec.add_dependency "rubyzip"
end
