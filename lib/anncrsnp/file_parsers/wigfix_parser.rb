class WigfixParser < FileParser
	def initialize(folder, chunk_size)
		super
		@start = 1
		@step = 1
	end

	def self.available?
		return TRUE
	end

	def self.format
		return 'wigfix'
	end

	def parse(line)
		#fixedStep chrom=chr11 start=60001 step=1
		if line.include?('fixedStep')
			line =~ /fixedStep chrom=(\S+) start=(\d+) step=(\d+)/
			if !@chrom.nil? && @chrom != $1 #We change of chromosome, we write the buffered coordinates
				#puts "=> #{@packs}\t#{@start}\tx"
				#puts @coords.first.inspect
				#puts @coords.last.inspect
				write_compressed_data
				@coords = []
			end
			@chrom = $1
			last_start = @start
			@start = $2.to_i
			diff = @start - last_start #Create dummy files to fill gaps on coordinate scores
			if diff >= @chunk_size
				(diff/@chunk_size).times do 
					#puts "=> #{@packs}\t#{@start}\td"
					#puts @coords.first.inspect
					#puts @coords.last.inspect
					write_compressed_data
					@coords = []
				end
			else
				if @start/@chunk_size != last_start/@chunk_size #Current coordinate belongs to another pack that the previous, write the buffered coordinates
					#puts "=> #{@packs}\t#{@start}\te"
					#puts @coords.first.inspect
					#puts @coords.last.inspect
					write_compressed_data
					@coords = []
				end
			end
			@step = $3.to_i
		else
			if @start % @chunk_size == 0 # We have reached the chun size, write it to disk
				#puts "=> #{@packs}\t#{@start}\tl"
				#puts @coords.first.inspect
				#puts @coords.last.inspect
				write_compressed_data
				@coords = []
			end
			@coords << [@start, line.to_f]
			@start += @step
		end
	end

	def get_data
		return @coords
	end
end