CHR = 0
START = 1
ENDING = 2
ID = 3

class Dataset
	def initialize(header = [])
		@all_record = []
		@metadata = {}
		add_metadata(:header, [:chr, :start, :ending, :id].concat(header))
	end

	def add_record(fields_array, add_start = 0, add_stop = 0) # Fixed col => 0 -> chr, 1 -> start, 2 -> end, 3 -> id
		fields_array[START] = fields_array[START].to_i + add_start
		fields_array[ENDING] = fields_array[ENDING].to_i + add_stop
		@all_record << fields_array
	end

	def add_record_batch(records_array)
		records_array.each do |record|
			add_record(record)
		end
	end

	def get_elements_on_position(chr, start, ending)
		results = []
		@all_record.each do |record|
			if record[CHR] == chr
				if (record[START] >= start && record[START] <= ending) || (record[ENDING] >= start && record[ENDING] <= ending) 
					results << record
				end
			end
		end
		return results
	end

	def each_record
		@all_record.each do |record|
			yield(record)
		end
	end

	def length
		return @all_record.length
	end

	def first
		return @all_record.first
	end

	def last
		return @all_record.last
	end

	def numeric_filter(col_name, thresold)
		index = get_metadata(:header).index(col_name)
		@all_record.select!{|rec| rec[index].to_f >= thresold }
	end

	def drop_columns(col_names)
		drop_index = col_names.map{|name| get_metadata(:header).index(name)}
		each_record do |rec|
			drop_index.reverse_each do |ind|
				rec.delete_at(ind)
			end
		end
		add_metadata(:header, get_metadata(:header) - col_names)
	end
	
	def extract_records_by_coordinates(dataset, results_column_name)
		added_fields = get_metadata(:added_fields)
		if added_fields
			added_fields[results_column_name] = dataset.get_metadata(:header)
		else
			added_fields = {results_column_name => dataset.get_metadata(:header)}
		end
		add_metadata(:added_fields, added_fields)
		add_metadata(:header, get_metadata(:header).concat([results_column_name])) # Add new column to dataset
		each_record{|record|

			record << dataset.get_elements_on_position(record[CHR], record[START], record[ENDING])
		}
	end

	def empty?
		return @all_record.empty?
	end

	def get_metadata(keyword)
		return @metadata[keyword]
	end

	def add_metadata(keyword, value)
		return @metadata[keyword] = value
	end

	def write(output_path, output_format)
		path = "#{output_path}.#{output_format}"
		if output_format == 'html'
			write_html(path)
		end
	end
	def write_html(path)
		output_file = File.open(path, 'w')
		# Header
		output_file.puts "<html>",
						"<header>",
						"</header>",
						"<body>"

		#Table
		output_file.puts '<table border=1>'
		output_file.puts write_html_table_header
		each_record{|record|
			output_file.puts create_record_template(record)
		}
		output_file.puts "</table>"

		# Footer
		output_file.puts "</body>",
						"</html>"

		output_file.close
	end

	def create_record_template(record)
		record_template = []
		record_lentgh = record.select{|field| field.class == Array }.map{|field| field.length}.max
		record_lentgh = 1 if record_lentgh.nil? || record_lentgh == 0
		record_lentgh.times do
			record_template << Array.new(@fields_number){" "}
		end
		field_position = 0
		record.each_with_index do |field, column_number|
			if field.class != Array
				record_template[0][field_position] = field
				field_position += 1
			else
				field.each_with_index do |added_record, raw|
					added_record.each_with_index do |record_field, col|
						record_template[raw][col+field_position+1] = record_field
					end 
				end

				field_position += @html_header[column_number- @html_header.first.last.length+1].last.length
				#field_position += @html_header[get_metadata(:header)[column_number]].length
			end
		end
		record_html = ""
		record_template.each do |row|
			record_html << "<tr><td>#{row.join("</td><td>")}</td></tr>\n"
		end
		return record_html
	end

	def write_html_table_header
		@html_header = []
		added_fields = get_metadata(:added_fields)
		@html_header << [ get_metadata(:classification) ,  get_metadata(:header) - added_fields.keys]
		puts @html_header.inspect
		added_fields.each do |classification, fields|
			@html_header << [classification, fields]
		end

		puts @html_header.inspect

		main_header = ""
		fields_header = ""
		@fields_number = 0
		@html_header.each do |classification, fields|
			main_header << "<td colspan=\"#{fields.length}\">#{classification.to_s}"
			fields_header << "<td>#{fields.map{|h| h.to_s}.join("</td><td>")}</td>"
			@fields_number += fields.length
		end

		return "<tr>#{main_header}</tr>\n<tr>#{fields_header}</tr>"
	end
end