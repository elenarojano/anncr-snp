require 'json'

# MyWorkerManager class is used to implement the methods
# to send and receive the data to or from workers
class PreprocessingManager < ScbiMapreduce::WorkManager

  ######################################################################################################
  ## MANAGER BASIC METHODS
  ######################################################################################################

  # init_work_manager is executed at the start, prior to any processing.
  # You can use init_work_manager to initialize global variables, open files, etc...
  # Note that an instance of MyWorkerManager will be created for each
  # worker connection, and thus, all global variables here should be
  # class variables (starting with @@)
  def self.init_work_manager(options)
    @@options = options
    $LOG.info 'Load genomic features links'
    @@features = load_links(options[:file])
    $LOG.info "Loaded #{@@features.length} genomic features links"

    # FEATURE DIRECTORIES
    @@features.each do |feature|
      ft_folder = File.join(@@options[:preprocessed_data], feature[1]) #feature name
      ft_temp_folder = File.join(@@options[:temp], feature[1])
      Dir.mkdir(ft_folder) if !Dir.exist?(ft_folder)
      Dir.mkdir(ft_temp_folder) if !Dir.exist?(ft_temp_folder)
    end
    @@processed_features = 0
  end

  # end_work_manager is executed at the end, when all the process is done.
  # You can use it to close files opened in init_work_manager
  def self.end_work_manager
    File.open(File.join(@@options[:preprocessed_data], 'active_data'), 'w'){ |f| f.puts @@features.map{|f| f[1]}.uniq.join("\n")}
  end

  # worker_initial_config is used to send initial parameters to workers.
  # The method is executed once per each worker
  def worker_initial_config
    return @@options
  end

  # next_work method is called every time a worker needs a new work
  # Here you can read data from disk
  # This method must return the work data or nil if no more data is available
  def next_work
    begin
      if @@processed_features >= @@features.length
        e = nil # worker signal disconect
      else
        e = @@features[@@processed_features]
      end

      @@processed_features += 1
    rescue Exception => e  
      puts e.message  
      puts e.backtrace

    end
    return e

  end


  # work_received is executed each time a worker has finished a job.
  # Here you can write results down to disk, perform some aggregated statistics, etc...
  def work_received(results)

    # write_data_to_disk(results)
  end

  ######################################################################################################
  ## CUSTOM ADDITIONAL METHODS
  ######################################################################################################

  def self.load_links(file_path)
    features = []
    File.open(file_path).each do |line|
      line.chomp!
      link, feature, cols, header, format = line.split("\t")
      features << [link, feature, cols.split(',').map{|col| col.to_i}, header.to_i, format]
    end 
    return features
  end

end
