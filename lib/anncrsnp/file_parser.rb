require 'yajl'
class FileParser
	@@parsers = {}
	def self.get_descendants
		return ObjectSpace.each_object(Class).select { |klass| klass < self }
	end

	def self.load
		path_parsers = File.join(File.dirname(__FILE__), 'file_parsers')
		Dir.glob(path_parsers+'/*').each do |parser|
			require parser
		end
		get_descendants.each do |descendant|
			@@parsers[descendant.format] = descendant if descendant.available?
		end
	end

	def self.select(format)
		return @@parsers[format]
	end

	########################################################################################
	## PARSER DEPENDANT METHODS
	########################################################################################
	def self.available?
		return FALSE
	end

	def self.format
		return 'master'
	end

	def initialize(folder, chunk_size)
		@folder = folder
		@chunk_size = chunk_size
		@chrom = nil
		@coords = []
		@packs = 0
	end

	def parse(line)

	end

	def write_compressed_data
		p = @packs * @chunk_size
		gz_path = File.join(@folder, "#{@chrom}_#{p}.gz")
        Zlib::GzipWriter.open(gz_path) do |writer|
          Yajl::Encoder.encode(@coords, writer)
        end
        @packs += 1
    end
end