require 'dataset'

def parseUCSCformat(file, header, skip_first_col = TRUE, add_start = 0, add_stop = 0)
	dataset = Dataset.new(header)
	File.open(file).each do |line|
		line.chomp!
		fields = line.split("\t") 
		bin_signal = fields.shift if skip_first_col
		dataset.add_record(fields, add_start, add_stop)
	end
	return dataset
end

def parseUCSCrefseqformat(file, header, skip_first_col = TRUE, add_start = 0, add_stop = 0)
	dataset = Dataset.new(header)
	File.open(file).each do |line|
		line.chomp!
		fields = line.split("\t") 
		bin_signal = fields.shift if skip_first_col
		fields = [fields[1], fields[3], fields[4], fields[11], fields[0], fields[2], fields[5], fields[6], fields[7], fields[8], fields[9], fields[10], fields[12], fields[13], fields[14]]
		dataset.add_record(fields, add_start, add_stop)
	end
	return dataset
end

def parseDENdbCSVformat(file, header)
	dataset = Dataset.new(header)
	File.open(file).each do |line|
		line.chomp!
		fields = line.split(",")
		fields = [fields[1], fields[2], fields[3], fields[0]].concat(fields[4..11])
		dataset.add_record(fields)
	end
	return dataset
end