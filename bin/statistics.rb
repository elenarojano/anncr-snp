#! /usr/bin/env ruby
require 'scbi_plot'
#METHODS
#----------
def load_snp_data(input_file, fields_length)
	snp_storage = {}
	index = {}
	counter = 0
	File.open(input_file).each do |line|
		line.chomp!
		fields = line.split("\t")
		snp_fields = fields.shift(fields_length) #in fields you store the genomic factors (histone modif, tfbs...)
		if counter == 0
			fields.each_with_index do |category, position|
				index[category] = position
			end
		else
			categories = {
							"HistoneModification" => [], 
							"tfbs" => [], 
							"dnaseData" => [],
							"methylationData" => [],
							"ConservedRegions" => [],
							"Enhancers" => [],
							"DENdbEnhancers" => [], 
							"SuperEnhancers" => [],
							"ORegAnnoTFBS" => [],
							"ORegAnnoRegulatoryElements" => []
						}
			categories.each do |category_name, category_value|
				column_position = index[category_name]
				if !column_position.nil?
					snp_category_values = fields[column_position]
					category_value.concat(snp_category_values.split(',')) if snp_category_values != '-' 
				end	
			end
			snp_storage[snp_fields[0]] = categories
					
			#bloque de código para tratar la información de los snps
		end
		counter += 1
	end
	return snp_storage
end

def snp_calculate_stats(snp_storage)
	snp_percentage = {
							"HistoneModification" => 0, 
							"tfbs" => 0, 
							"dnaseData" => 0,
							"methylationData" => 0,
							"ConservedRegions" => 0,
							"Enhancers" => 0,
							"DENdbEnhancers" => 0,
							"SuperEnhancers" => 0,
							"ORegAnnoTFBS" => 0,
							"ORegAnnoRegulatoryElements" => 0
						}
	snp_storage.each do |snp_name, annotations|
		annotations.each do |annotation_category, annotation_value|
			if !annotation_value.empty?
				snp_percentage[annotation_category] += 1 #possible error point! 	
			end
		end
	end
	total_snps = snp_storage.length.to_f
	snp_percentage.each do |annotation_category, true_positive_number|
		percentage = true_positive_number / total_snps * 100
		snp_percentage[annotation_category] = percentage
	end
	return snp_percentage
end

def create_histogram(snp_percentage, name)
	# create Histogram
	p=ScbiPlot::Histogram.new(name,'SNPs genomic region annotations')
	# add x axis data
	p.add_x(snp_percentage.keys)
	puts snp_percentage.keys.inspect
	# add y axis data
	p.add_y(snp_percentage.values)
	puts snp_percentage.values.inspect
	# generate graph
	p.do_graph
end


def snp_calculate_stats_with_reference(snp_storage, snp_storage_reference)
	snp_percentage = {
							"HistoneModification" => 0, 
							"tfbs" => 0, 
							"dnaseData" => 0,
							"methylationData" => 0,
							"ConservedRegions" => 0,
							"Enhancers" => 0,
							"DENdbEnhancers" => 0,
							"SuperEnhancers" => 0,
							"ORegAnnoTFBS" => 0,
							"ORegAnnoRegulatoryElements" => 0
						}

	snp_storage_reference.each do |snp_name_ref, annotations_ref|
		query = snp_storage[snp_name_ref]
		if !query.nil?
			annotations_ref.each do |annotation_category_ref, annotation_value_ref|
				annotation_value = query[annotation_category_ref]
				if annotation_comparison(annotation_value_ref, annotation_value, annotation_category_ref)
					snp_percentage[annotation_category_ref] += 1 	
				end
			end
		end
	end

	total_snps = snp_storage_reference.length.to_f
	snp_percentage.each do |annotation_category, true_positive_number|
		percentage = true_positive_number / total_snps * 100
		snp_percentage[annotation_category] = percentage
	end
	return snp_percentage
end

def annotation_comparison(annotation_value_ref, annotation_value, annotation_category_ref)
	result = false
	annotation_value_ref.uniq!
	annotation_value.uniq!
	#puts "#{annotation_value_ref.inspect} => #{annotation_value}" if annotation_category_ref == 'dnaseData'
	if annotation_value_ref.sort == annotation_value.sort 
		result = true
	elsif annotation_category_ref == 'dnaseData' && 
		!annotation_value.empty? 
		result = true
	elsif annotation_category_ref == 'tfbs'	
		if !(annotation_value_ref & annotation_value).empty? || annotation_value.length >= 5 			
			result= true
		end
	elsif annotation_category_ref == 'methylationData' &&
		!annotation_value.empty?
		result = true	
	elsif annotation_category_ref == 'HistoneModification'
		annotation_value_ref = annotation_value_ref.map{|an| 
			if /(H\d+K\d+)\w*/ =~ an
				$1
			else
				an
			end
		}.uniq
		annotation_value = annotation_value.map{|an| 
			if /(H\d+K\d+)\w*/ =~ an
				$1
			else
				an
			end
		}.uniq
		if !(annotation_value_ref & annotation_value).empty? || annotation_value.length >= 5 			
			result= true
		end	
	elsif annotation_category_ref == 'ConservedRegions' && 
		!annotation_value.empty? 
		result = true
	elsif annotation_category_ref == 'Enhancers' &&
		!annotation_value.empty?
		result = true
	elsif annotation_category_ref == 'DENdbEnhancers' &&
		!annotation_value.empty?
		result = true
	elsif annotation_category_ref == 'SuperEnhancers' &&
		!annotation_value.empty?
		result = true
	elsif annotation_category_ref == 'ORegAnnoTFBS' &&
		!annotation_value.empty?
		result = true
	elsif annotation_category_ref == 'ORegAnnoRegulatoryElements' &&
		!annotation_value.empty?
		result = true
	end
	return result 
end

#MAIN
#----------
fields_length = 5
fields_length = ARGV[2].to_i if !ARGV[2].nil?

snp_storage = load_snp_data(ARGV[0], fields_length)
if !ARGV[1].nil? && ARGV[1].downcase != 'false'
	snp_storage_reference = load_snp_data(ARGV[1])
	snp_percentage = snp_calculate_stats_with_reference(snp_storage, snp_storage_reference) 
else
	snp_percentage = snp_calculate_stats(snp_storage)
end
snp_percentage.each do |category_name, percentage|
	puts "#{category_name.capitalize}\t#{percentage}\t#{ARGV[3]}"
end
# file_name = File.basename(ARGV[0], ".txt")
# graph_name = file_name + ".png"
# create_histogram(snp_percentage, graph_name)
