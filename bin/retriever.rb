#! /usr/bin/env ruby
ROOT_PATH = File.dirname(__FILE__)
$: << File.expand_path(File.join(ROOT_PATH, '..', 'lib', 'anncrsnp'))


require 'optparse'
require 'scbi_mapreduce'
require 'preprocessing_manager'
require 'position_selection_manager'

#####################################################################
### OPTPARSE
#####################################################################

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: #{File.basename(__FILE__)} [options]"

  ### PARALELISATION OPTIONS
  #####################################################################
  options[:server_ip] = '0.0.0.0'
  opts.on( '-s', '--server IP', 'Server ip. You can use a partial ip to select the apropriate interface' ) do |server_ip|
    options[:server_ip] = server_ip
  end

 # server port
  options[:port] = 0 # any free port
  opts.on( '-p', '--port PORT', 'Server port. If set to 0, an arbitrary empty port will be used') do |port|
    options[:port] = port.to_i
  end

  # set number of workers. You can also provide an array with worker names.
  # Those workers names can be read from a file produced by the existing
  # queue system, if any.
  options[:workers] = 2
  opts.on( '-w', '--workers COUNT', 'Number of workers, or file containing machine names to launch workers with ssh' ) do |workers|
    if File.exists?(workers)
      # use workers file
      options[:workers] = File.read(workers).split("\n").map{|w| w.chomp}
    else
      begin
        options[:workers] = Integer(workers)
      rescue
        STDERR.puts "ERROR:Invalid workers parameter #{options[:workers]}"
        exit
      end
    end
  end

  # chunk size
  options[:chunk_size] = 1
  opts.on( '-g', '--group_size chunk_size', 'Group sequences in chunks of size <chunk_size>' ) do |cs|
    options[:chunk_size] = cs.to_i
  end

  ### EXECUTION OPTIONS
  #####################################################################
  options[:index_size] = 1000000
  opts.on( '-x', '--index_size INTEGER', 'Size of genomic features data packs' ) do |is|
    options[:index_size] = is.to_i
  end

  options[:file] = nil
  opts.on("-f", "--file-links PATH", "Input file with links to retrieve data") do |f|
    options[:file] = f
  end

  options[:output] = 'data'
  opts.on("-o", "--output PATH", "Folder output path") do |f|
    options[:output] = f
  end

  options[:downloaded_only] = FALSE
  opts.on("--download_only", "Only download gemonic features files but not process them") do
    options[:downloaded_only] = TRUE
  end

  options[:no_auc] = FALSE
  opts.on("--no_auc", "No calculate auc by each genomic feature") do
    options[:no_auc] = TRUE
  end

  options[:selected_positions] = nil
  opts.on("--selected_positions PATH", "Tabular file with chromosome (as chrN) and base 1 coordinates. Optionally a third field can be added with 0/1 values for positive/negative groups") do |selected|
    options[:selected_positions] = selected
  end

end.parse!

#####################################################################
### MAIN
#####################################################################

# GENERAL FOLDER
Dir.mkdir(options[:output]) if !Dir.exist?(options[:output])

# MAPREDUCE LAUNCHING
##########################################################
$LOG = Logger.new(STDOUT)
$LOG.datetime_format = "%Y-%m-%d %H:%M:%S"
# Genomic feature data downloading and preprocessing
#-----------------------------------------------------------------------------
if !options[:file].nil? 
	if  File.exists?(options[:file])
		temp = File.join(options[:output], 'temp')
		options[:temp] = temp
		Dir.mkdir(temp) if !Dir.exist?(temp)
		preprocessed_data = File.join(options[:output], 'preprocessed_data')
		options[:preprocessed_data] = preprocessed_data
		Dir.mkdir(preprocessed_data) if !Dir.exist?(preprocessed_data)
		
		$LOG.info 'Starting PREPROCESSING server'
		custom_worker_file = File.join(ROOT_PATH, '..', 'lib', 'anncrsnp', 'preprocessing_worker.rb')
		PreprocessingManager.init_work_manager(options)

		mgr = ScbiMapreduce::Manager.new( options[:server_ip], options[:port], options[:workers], PreprocessingManager, custom_worker_file, STDOUT) # launch processor server
		mgr.chunk_size = options[:chunk_size]
		mgr.start_server # start processing
		$LOG.info 'Closing PREPROCESSING server'
	else
		puts "Links file not exists\n#{options[:file]}"
		Process.exit()
	end
end

# Genomic feature data position selection
#-----------------------------------------------------------------------------
if !options[:selected_positions].nil? 
	if File.exist?(options[:selected_positions])
		selected_positions_folder = File.join(options[:output], 'selected_positions')
		options[:selected_positions_folder] = selected_positions_folder
		Dir.mkdir(selected_positions_folder) if !Dir.exist?(selected_positions_folder)

		$LOG.info 'Starting POSITION_SELECTION server'
		custom_worker_file = File.join(ROOT_PATH, '..', 'lib', 'anncrsnp', 'position_selection_worker.rb')
		PositionSelectionManager.init_work_manager(options)

		mgr = ScbiMapreduce::Manager.new( options[:server_ip], options[:port], options[:workers], PositionSelectionManager, custom_worker_file, STDOUT) # launch processor server
		mgr.chunk_size = options[:chunk_size]
		mgr.start_server # start processing
		$LOG.info 'Closing POSITION_SELECTION server'
	else
		puts "File with selected positions not exists:\n#{options[:selected_positions]}"
		Process.exit()
	end
end

