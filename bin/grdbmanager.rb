#! /usr/bin/env ruby

ROOT_PATH = File.dirname(__FILE__)
$: << File.expand_path(File.join(ROOT_PATH, '..', 'lib', 'anncrsnp'))
$: << File.expand_path(File.join(ROOT_PATH, '..', 'lib', 'anncrsnp', 'parsers'))

require 'optparse'
require 'ucscparser'
require 'dataset'
require 'sqlite3'

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: #{__FILE__} [options]"

  options[:data] = nil
  opts.on("-d", "--data_directory PATH", "Directory used to extract data") do |data|
    options[:data] = data
  end

  options[:create_sql] = FALSE
  opts.on("-s", "--create_sql", "Create SQL DB") do
    options[:create_sql] = TRUE
  end

  options[:output_path] = "genomic_data.sqlite"
  opts.on("-o", '--output_path PATH', 'Output path for DB') do |output_path|
  	options[:output_path] = output_path
  end

  options[:verbose] = nil
  opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
    options[:verbose] = v
  end

end.parse!



all_data = {}
if File.exist?(options[:data])
	Dir.glob(File.join(options[:data],'*.{txt,bed,csv}')).each do |file| # we get the path to each file on directory
		current_file = File.basename(file)
### Definitive sources
#If bin field from UCSC doesn't exist, put FALSE as input data to parseUCSCformat method
#OMIT IN HEADER THE 4 FIRST COLUMNS
		if current_file == "wgEncodeAwgDnaseMasterSites.bed"
			header = [:score, :floatScore, :sourceCount, :sourceIds]
			current_dataset = parseUCSCformat(file, header, FALSE, 1, 0)
			current_dataset.numeric_filter(:sourceCount, 2)
			current_dataset.drop_columns(header)
			current_dataset.add_metadata(:classification, 'DNAseHS')
			all_data['dnaseData'] = current_dataset			
		elsif current_file == "wgEncodeHaibMethyl450Ag04449SitesRep1.bed"
			header = [:score, :strand, :thickStart, :thickEnd, :itemRgb]
			current_dataset = parseUCSCformat(file, header, FALSE, 1, 0)
			current_dataset.drop_columns(header)
			current_dataset.add_metadata(:classification, 'Methylation_sites')
			all_data['methylationData'] = current_dataset
		elsif current_file == "snp144Common.txt" # current_file == "test.txt"
			header = [:score, :strand, :refNCBI, :refUCSC, :observed, :molType, :class, :valid, :avHet, :avHetSE, :func, :locType, :weight, :exceptions, :submitterCount, :submitters, :alleleFreqCount, :alleles, :alleleNs, :alleleFreqs, :bitfields]
			current_dataset = parseUCSCformat(file, header, TRUE, 1, 0)
			current_dataset.drop_columns([:score, :strand, :refNCBI, :refUCSC, :observed, :molType, :valid, :avHet, :avHetSE, :locType, :weight, :exceptions, :submitterCount, :submitters, :alleleFreqCount, :alleles, :alleleNs, :alleleFreqs, :bitfields])
			current_dataset.add_metadata(:classification, 'SNP')
			all_data['snpDbSnp'] = current_dataset
		elsif current_file == "refGene.txt" 
			header = [:name, :strand, :cdsStart, :cdsEnd, :exonCount, :exonStarts, :exonEnds, :score, :cdsStartStat, :cdsEndStat, :exonFrames]
			current_dataset = parseUCSCrefseqformat(file, header, TRUE, 1, 0)
			current_dataset.drop_columns(header)
			current_dataset.add_metadata(:classification, 'gene')
			all_data['gene'] = current_dataset
		elsif current_file == "TFBSMasterSites.txt" #Must be generated with "masterfeatures.rb tfbs/files.txt antibody import_data/TFBSMasterSites.txt tfbs/"
			header = []
			current_dataset = parseUCSCformat(file, header, FALSE, 1, 0)
			current_dataset.add_metadata(:classification, 'TFBS')
			all_data['tfbs'] = current_dataset
		elsif current_file == "HistoneModMasterSites.txt" #Must be generated with "masterfeatures.rb tfbs/files.txt antibody import_data/TFBSMasterSites.txt tfbs/"
			header = []
			current_dataset = parseUCSCformat(file, header, FALSE, 1, 0)
			current_dataset.add_metadata(:classification, 'HistoneModification')
			all_data['HistoneModification'] = current_dataset				
		elsif current_file == "46waycons.txt" 
			header = [:span, :count, :offset, :file, :lowerLimit, :dataRange, :validCount, :sumData, :sumSquares]
			current_dataset = parseUCSCformat(file, header, TRUE, 1, 0)
			current_dataset.drop_columns(header)
			current_dataset.add_metadata(:classification, 'ConservedRegions')
			all_data['ConservedRegions'] = current_dataset
		elsif current_file == "enhancer_tss_associations.bed" 
			header = [:score, :strand, :enh_start, :enh_stop, :array, :index, :val1, :val2]
			current_dataset = parseUCSCformat(file, header, FALSE, 0, 0)
			current_dataset.drop_columns(header)
			current_dataset.add_metadata(:classification, 'Enhancers')
			all_data['Enhancers'] = current_dataset
		elsif current_file == "enhancers.csv" 
			header = [:cell_line, :index1, :index2, :index3, :index4, :index5, :index6, :index7]
			current_dataset = parseDENdbCSVformat(file, header)
			current_dataset.drop_columns(header)
			current_dataset.add_metadata(:classification, 'DENdbEnhancers')
			all_data['DENdbEnhancers'] = current_dataset
		elsif current_file == "all_hg19_bed.bed" 
			header = [:counter]
			current_dataset = parseUCSCformat(file, header, FALSE, 0, 0)
			current_dataset.drop_columns(header)
			current_dataset.add_metadata(:classification, 'SuperEnhancers')
			all_data['SuperEnhancers'] = current_dataset		
		elsif current_file == "oreganno_tfbs.txt"
			header = []
			current_dataset = parseUCSCformat(file, header, TRUE, 0, 0)
			current_dataset.drop_columns(header)
			current_dataset.add_metadata(:classification, 'ORegAnnoTFBS')
			all_data['ORegAnnoTFBS'] = current_dataset
		elsif current_file == "oreganno_regulatory.txt"
			header = []
			current_dataset = parseUCSCformat(file, header, TRUE, 0, 0)
			current_dataset.drop_columns(header)
			current_dataset.add_metadata(:classification, 'ORegAnnoRegulatoryElements')
			all_data['ORegAnnoRegulatoryElements'] = current_dataset
		#UNCOMMENT FOR INCLUDE GTEX DATA FROM UCSC. THIS FILE DOESN'T HAVE BIN FIELD!!
		# elsif current_file == "gtexGene.txt" 
		# 	header = [:name, :score, :strand, :geneId, :geneType, :expCount, :expScores]
		# 	current_dataset = parseUCSCrefseqformat(file, header, TRUE, 1, 0)
		# 	current_dataset.drop_columns(header)
		# 	current_dataset.add_metadata(:classification, 'GTEx')
		# 	all_data['GTEx'] = current_dataset
		end
	end
end

if options[:create_sql]
	commands = []
	if !File.exists?(options[:output_path])
		commands << "CREATE TABLE GenomicRange(
			bin,
			chr,
			start,
			end,
			type,
			name,
			AnnotationId
		)"
	 	commands << "CREATE TABLE Annotation(
			value,
			AnnotationTypeId
		)"
		commands << "CREATE TABLE AnnotationType(
			type
		)"
		#File.delete(options[:output_path]) 
	end

	DB = SQLite3::Database.new( options[:output_path] )
	commands.each do |cmd|
		DB.execute(cmd)
	end
	# Import data process speed up configuration
	DB.execute("PRAGMA synchronous = OFF;")
	DB.execute("PRAGMA journal_mode = MEMORY;")

	# Creating memory indexes for incremental updates
	annotation_type_index = DB.execute("SELECT rowid, * FROM AnnotationType").group_by {|r| r[1]}
	annotation_index = DB.execute("SELECT rowid, * FROM Annotation").group_by {|r| r[1]}

	all_data.each do |class_data, dataset|
		puts "#{class_data} import started"
		# Save and create AnnotationType data
		#------------------------------------------------------------------
		header = dataset.get_metadata(:header)
		annotation_type = header.map{|h| h.to_s}
		annotation_type.shift(4)
		if !commands.empty?
			records = annotation_type
		else
			records = annotation_type.select{|at| annotation_type_index[at].first.nil?} 
		end

		DB.transaction do |db|
			db.prepare("INSERT INTO AnnotationType(type) VALUES(?)") do |smnt| # Precompile query for speed up process
				records.each do |rec|
					smnt.execute(rec)
				end
			end
		end		
		annotation_type_index = DB.execute("SELECT rowid, * FROM AnnotationType").group_by {|r| r[1]} if !records.empty?

		# Save and create AnnotationType data
		#------------------------------------------------------------------
		annotations = {}
		annotation_type.each do |at|
			annotations[at] = {}
		end
		if dataset.first.length > 4
			dataset.each_record do |record|
				record[5..record.length - 1].each_with_index do |annotation, i|
						annotations[annotation_type[i]][annotation] = nil
				end
			end
		end

		records = []
		annotations.each do |annotation_type, values|
			annotation_type_id = annotation_type_index[annotation_type].first.first
			if !commands.empty?
				records = records.concat(values.keys.map{|v| [v, annotation_type_id]})
			else
				records = records.concat(values.keys.select{|v| annotation_index[v].nil? }.map{|v| [v, annotation_type_id]}) 
			end
		end

		DB.transaction do |db|
			db.prepare("INSERT INTO Annotation(value, AnnotationTypeId) VALUES(?, ?)") do |smnt|
				records.each do |rec|
					smnt.execute(rec[0], rec[1])
				end
			end
		end		
		annotation_index = DB.execute("SELECT rowid, * FROM Annotation").group_by {|r| r[1]} if !records.empty?

		# Save and create GenomicRange data
		#------------------------------------------------------------------
		DB.transaction do |db|
			db.prepare("INSERT INTO GenomicRange(bin, chr, start, end, type, name, AnnotationId) VALUES(?, ?, ?, ?, ?, ?, ?)") do |smnt|
				dataset.each_record do |record|
					region_data = record.shift(4)
					annotation_ids = []
					record.each do |annotation|
						id = annotation_index[annotation]
						annotation_ids << id.first.first if !id.nil?
					end
					smnt.execute(
						region_data[1]/10000,
						region_data[0],
						region_data[1],
						region_data[2],
						class_data,
						region_data[3],
						annotation_ids.join(',')
						)
				end
			end
		end
		puts "#{class_data} import finished"
	end
end
DB.execute("CREATE INDEX name_index ON GenomicRange (name)")
DB.execute("CREATE INDEX bin_index ON GenomicRange (bin)")
DB.close
