#! /usr/bin/env ruby

ROOT_PATH = File.dirname(__FILE__)
$: << File.expand_path(File.join(ROOT_PATH, '..', 'lib', 'anncrsnp'))
$: << File.expand_path(File.join(ROOT_PATH, '..', 'lib', 'anncrsnp', 'parsers'))

require 'optparse'
require 'dataset'
require 'sqlite3'
require 'benchmark'
require 'zip'
require 'open-uri'

######################################################################################################################
## METHODS
######################################################################################################################

# QUERING METHODS
#----------------------------------------------------
def query_coordinates(coords, flanking_region)
	genomic_regions = []
  coords.each do |chr, start, stop|
    start = start.to_i-flanking_region
    start = 0 if start < 0
    stop = stop.to_i+flanking_region 
  	#bins = (start/10000).upto(stop/10000).to_a
    #genomic_regions = $db.execute("SELECT * FROM  GenomicRange WHERE chr=? AND (start>=? AND end<=? )", chr, start, stop)
    #genomic_regions = $db.execute("SELECT * FROM  GenomicRange WHERE chr=? AND (start>=? AND end<=? )", chr, start, stop)
    #genomic_regions = $db.execute("SELECT * FROM  GenomicRange WHERE chr=? AND bin IN(#{Array.new(bins.length, '?').join(',')})", chr, bins)
  	local_genomic_regions = $db.execute("SELECT * FROM  GenomicRange WHERE chr=? AND (bin BETWEEN ? AND ?)", chr, start/10000, stop/10000)
    #puts "QUERY_COORDS",'--------------',genomic_regions.inspect
    local_genomic_regions.select!{|g_reg|
        (g_reg[2] >= start && g_reg[2] <= stop) || #ge_reg start is in region  
        (g_reg[3] >= start && g_reg[3] <= stop) || #ge_reg end is in region
        (g_reg[2] <= start && g_reg[3] >= stop) #region is in ge_reg
    }
    genomic_regions.concat(local_genomic_regions)
  end
	return genomic_regions
end

def query_name(name)
  genomic_regions = []
  genomic_regions = $db.execute("SELECT * FROM  GenomicRange WHERE name=?", name)
  #puts "QUERY_NAME",'--------------',genomic_regions.inspect
  return genomic_regions
end

def query_name_and_region(name, flanking_region)
  genomic_regions_by_name = [] 
  name.each do |reg_name|
    genomic_regions = genomic_regions_by_name.concat(query_name(reg_name))
  end
  #puts "QUERY_NAME_AND_REGION",'--------------',genomic_regions_by_name.inspect
  genomic_regions = query_coordinates(genomic_regions_by_name.map{|g_reg| g_reg[1..3]}, flanking_region) #g_reg[1..3] => chr, start, stop
  genomic_regions.uniq! #subqueries can retrie the same elements and may repeat results.
  return genomic_regions
end

# REPORTING METHODS
#----------------------------------------------------
def simple_list(genomic_regions, output_path, output_format)
  path = output_path + '_simple_list.' + output_format
  if output_format == 'gff'
    simple_list_gff(genomic_regions, path)
  else 
    simple_list_html(genomic_regions, path)
  end
end

def simple_list_html(genomic_regions, path) 
  report = File.open(path, 'w')
  report.puts '<HTML>',
    '<header>',
    '</header>',
    '<body>',
    '<table border=1>',
    '<tr>',
    '<th>Chromosome</th><th>Start</th><th>End</th><th>Region type</th><th>Region id</th>'
    '</tr>'
  genomic_regions.each do |bin, chr, start, stop, type, name, annotationid|
    report.puts '<tr>',
      "<td>#{chr}</td><td>#{start}</td><td>#{stop}</td><td>#{type}</td><td>#{name}</td>"
      '</tr>'
  end
  report.puts '</table>', 
    '</body>',
    '</HTML>'
  report.close
end

def simple_list_gff(genomic_regions, path) #use generated file on http://genometools.org/cgi-bin/annotationsketch_demo.cgi
  report = File.open(path, 'w')
  report.puts '##gff-version 3'
  main_chr = genomic_regions.first[1]
  min_start = genomic_regions.map{|g_reg| g_reg[2]}.min
  max_stop = genomic_regions.map{|g_reg| g_reg[3]}.max
  add_region = ((max_stop - min_start).abs * 0.05).to_i
  region_start = min_start - add_region
  region_start = 0 if region_start < 0
  region_stop = max_stop + add_region
  report.puts "#{main_chr}\t#{File.basename(__FILE__)}\tchromosome\t#{region_start}\t#{region_stop}\t.\t.\t.\tID=#{main_chr}"
  genomic_regions.each do |bin, chr, start, stop, type, name, annotationid|
    report.puts "#{chr}\t#{File.basename(__FILE__)}\t#{type}\t#{start}\t#{stop}\t.\t.\t.\tName=#{name}"
  end
  report.close
end

def get_uniq_ids_from_records(records)
 ids = {}
  records.each do |rec|
    annotation_ids = rec.last.split(',')
    rec[6] = annotation_ids
    if annotation_ids.first != ''
      annotation_ids.each do |annot_id|
        ids[annot_id] = nil
      end
    end
  end
  return ids
end

def load_annotations(genomic_regions)
  annotations = get_uniq_ids_from_records(genomic_regions)
  if !annotations.empty?
    db_annotations = $db.execute("SELECT rowid, * FROM Annotation WHERE rowid IN(#{Array.new(annotations.length, '?').join(',')})", annotations.keys)
    annotation_types = db_annotations.map{|db_an| db_an.last}.uniq
    if !annotation_types.empty?
      db_annotation_types = $db.execute("SELECT rowid, * FROM AnnotationType WHERE rowid IN(#{Array.new(annotation_types.length, '?').join(',')})", annotation_types)
      db_annotation_types = db_annotation_types.group_by {|r| r.first}
      db_annotations.each do |db_an|
        db_an[0] = db_an[0].to_s
        db_an[2] = db_annotation_types[db_an[2]].first.last
      end
      db_annotations.each do |annot|
        id = annot.shift
        annotations[id] = annot
      end
      genomic_regions.each do |g_reg|
        annot_ids = g_reg.last
        final_annot = {}
        if !annot_ids.empty?
          annot_ids.each do |id|
            value, type = annotations[id]
            final_annot[type] = value
          end
        end
        g_reg[6] = final_annot
      end
    end
  end
end

def generate_query_regions(coords)
  query_regions = []
  coords.each do |chr, start, stop|
    query_regions << [nil, chr, start, stop, 'query_coords', "Q_#{chr}_#{start}-#{stop}", ''] #bin, chr, start, stop, type, name, annot
  end
  return query_regions
end

def grouping_list(group, genomic_regions, output_path, output_format)
  path = output_path + '_grouping_list.' + output_format
  load_annotations(genomic_regions)
  main_regions = genomic_regions.select{|reg| reg[4] == group} 
  putative_overlapping_regions = genomic_regions.select{|reg| reg[4] != group}
  overlaping_index = get_overlapping_regions_batch(main_regions, putative_overlapping_regions)
  if output_format == 'html'
    grouping_list_html(overlaping_index, main_regions, putative_overlapping_regions, path)
  elsif output_format == 'txt'
    grouping_list_txt(overlaping_index, main_regions, putative_overlapping_regions, path)
  end
end

def grouping_list_txt(overlaping_index, main_regions, putative_overlapping_regions, path)
  overlaping_regions = []
  overlaping_index.values.flatten.uniq.each do |pos|
    overlaping_regions << putative_overlapping_regions[pos]
  end
  grouping_type = main_regions.first[4]
  basic_fields = ['Id', 'Chromosome', 'Start', 'Stop']
  header_structure = get_header({grouping_type => basic_fields}, main_regions + overlaping_regions)
  report = File.open(path, 'w')
  txt_header = ''
  header_structure.each do |region_type, annotations|
    if annotations.length > 0
      txt_header << annotations.map{|an| region_type + '.' + an}.join("\t") + "\t" 
    else
      txt_header << region_type + "\t"
    end
  end
  report.puts txt_header.chop
  main_regions.each_with_index do |main_region, position|
    local_overlapping_regions = overlaping_index[position].map{|pos| putative_overlapping_regions[pos]}
    report.print "#{main_region[5]}\t#{main_region[1]}\t#{main_region[2]}\t#{main_region[3]}\t"
    header_structure[grouping_type] = header_structure[grouping_type] - ['Id', 'Chromosome', 'Start', 'Stop']
    header_structure[grouping_type].each do |annotation_type|
      report.print "#{main_region.last[annotation_type]}\t"
    end
    header_structure.each do |region_type, annotation_types|
      next if region_type == grouping_type
      record = local_overlapping_regions.select{|r| r[4] == region_type} #array
      if record.empty?
        if annotation_types.length == 0
          report.print "-\t"
        else
          report.print "-\t"*annotation_types.length
        end
      else
        if annotation_types.length == 0
          report.print "#{record.map{|r| r[5]}.uniq.join(',')}\t"
        else
          annotation_types.each do |an_type|
            report.print "#{record.map{|r| r.last[an_type]}.uniq.join(',')}\t"
          end
        end
      end
    end
    report.puts
  end 
  report.close
end

def get_overlapping_regions_batch(main_regions, putative_overlapping_regions)
  index = {}
  main_regions.length.times do |n|
    index[n] = []
  end
  main_position = 0
  main_regions.each do |bin, chr, start, stop, type, name, annotations|
    over_position = 0
    putative_overlapping_regions.each do |bin_over, chr_over, start_over, stop_over, type_over, name_over, annotations_over|
      if chr == chr_over && 
        ((start >= start_over && start <= stop_over) || (stop >= start_over && stop <= stop_over))
          index[main_position] << over_position
      end
      over_position += 1 
    end
    main_position += 1
  end
  return index
end

def grouping_list_html(overlaping_index, main_regions, putative_overlapping_regions, path)
  overlaping_regions = []
  overlaping_index.values.flatten.uniq.each do |pos|
    overlaping_regions << putative_overlapping_regions[pos]
  end
  report = File.open(path, 'w')
  report.puts '<HTML>',
    '<header>',
    '</header>',
    '<body>',
    '<table border=1>'
  grouping_type = main_regions.first[4]
  basic_fields = ['Id', 'Chromosome', 'Start', 'Stop']
  header_structure = get_header({grouping_type => basic_fields}, main_regions + overlaping_regions)
  report.puts get_grouping_html_header(header_structure)
  header_structure[grouping_type] = header_structure[grouping_type] - basic_fields
  main_regions.each_with_index do |main_region, position|
    local_overlapping_regions = overlaping_index[position].map{|pos| putative_overlapping_regions[pos]}
    record_rows = get_max_overlapping_regions_by_type(local_overlapping_regions)
    rowspan = nil
    rowspan = " rowspan=#{record_rows}" if record_rows > 1
    report.puts '<tr>',
      "<td#{rowspan}>#{main_region[5]}</td>",
      "<td#{rowspan}>#{main_region[1]}</td>",
      "<td#{rowspan}>#{main_region[2]}</td>",
      "<td#{rowspan}>#{main_region[3]}</td>"
    header_structure[grouping_type].each do |annotation_type|
      report.puts "<td#{rowspan}>#{main_region.last[annotation_type]}</td>"
    end
    record_rows.times do
      header_structure.each do |region_type, annotation_types|
        next if region_type == grouping_type
        record = local_overlapping_regions.select{|r| r[4] == region_type}.first
        if record.nil?
          if annotation_types.length == 0
            report.puts "<td></td>"
          else
            report.puts "<td></td>"*annotation_types.length
          end
        else
          if annotation_types.length == 0
            report.puts "<td>#{record[5]}</td>"
          else
            annotation_types.each do |an_type|
              report.puts "<td>#{record.last[an_type]}</td>"
            end
          end
          local_overlapping_regions.delete(record)
        end
      end
      report.puts '</tr>'
    end
  end
  report.puts '</table>', 
    '</body>',
    '</HTML>'
  report.close
end

def get_max_overlapping_regions_by_type(local_overlapping_regions)
  res = 1
  local_overlapping_regions.group_by{|r| r[4]}.each do |region_type, regions|
    reg_length = regions.length
    res = reg_length if reg_length > res
  end 
  return res
end

def get_grouping_html_header(header_structure)
  main_header = "<tr>\n"
  sub_header = "<tr>\n"
  header_structure.each do |main_title, cols|
    main_header << '<th'
    main_header << " rowspan=2" if cols.length == 0
    main_header << " colspan=#{cols.length}" if cols.length > 1
    main_header << ">#{main_title}</th>\n"
    cols.each do |col|
      sub_header << "<th>#{col}</th>\n"
    end
  end
  main_header << "</tr>\n"
  sub_header << "</tr>\n"
  return main_header + sub_header
end

def get_header(header, genomic_regions)
  genomic_regions.each do |ge_reg|
    region_type = ge_reg[4]
    region_annotations = ge_reg.last.keys
    query = header[region_type]
    if query.nil?
      header[region_type] = region_annotations 
    else
      header[region_type] = query | region_annotations
    end  
  end
  return header
end

# DATABASE METHODS
#----------------------------------------------------
  
def download_database(database_path)
  out_path = File.dirname(database_path)
  puts "Downloading database in #{out_path}, please be patient..."
  zip_path = File.join(out_path, 'database.zip')
	# Code from https://www.ruby-forum.com/topic/4413829
	target = "http://bio-267-data.uma.es/database.zip"

	bytes_total = nil

	open(target, "rb",
     		:content_length_proc => lambda{|content_length|
       		bytes_total = content_length},
     		:progress_proc => lambda{|bytes_transferred|
       	if bytes_total
        	 # Print progress
         	print("\r#{bytes_transferred}/#{bytes_total}")
       	else
        	 # We don’t know how much we get, so just print number
        	 # of transferred bytes
        	 print("\r#{bytes_transferred} (total size unknown)")
       	end
     	}) do |page|
  	# Now the real operation
  		File.open(zip_path, "wb") do |file|
    			# The file may not fit into RAM entirely, so copy it
    			# chunk by chunk.
    			while chunk = page.read(1024)
      				file.write(chunk)
    			end
  		end
	end
	
  if File.exists?(zip_path)
  	 puts "\nDecompressing database..."
 	 Zip::File.open(zip_path) do |zip_file|
   		 zip_file.each do |entry|
      			entry.extract(database_path)
    		 end
  	end
  else
	puts "ERROR: #{zip_path} was not found"
	Process.exit
  end
  if File.exists?(database_path)
   File.delete(zip_path) 
  end
end

######################################################################################################################
## INPUT PARAMETER PARSING
######################################################################################################################
options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: #{File.basename(__FILE__)} [options]"

  options[:coords] = []
  opts.on("-c", '--region_coordinates STRING', 'Coordinates to make the search. Format: chrN:start:end') do |coords|
    coord_lines = []
    if File.exists?(coords) == FALSE
      coord_lines = coords.split(',') 
    else
      coord_lines = File.readlines(coords).map{|line| line.chomp} 
    end
    options[:coords] = coord_lines.map{|line| line.split(':')}.map{|coords| [coords[0], coords[1].to_i, coords[2].to_i ]}
  end

  options[:name] = []
  opts.on("-n", '--region_name STRING', 'Search region by name') do |region|
    if File.exists?(region) == FALSE
      options[:name] = region.split(',') 
    else
      options[:name] = File.readlines(region).map{|line| line.chomp}
    end
  end

  options[:input_names_only] = FALSE
  opts.on("-i", '--input_names_only', 'Show info about only input data') do
    options[:input_names_only] = TRUE 
  end

  options[:flanking_region] = 0
  opts.on("-F", '--flanking_region INTEGER', 'Flanking region to search aroun the elements') do |flanking_region|
    options[:flanking_region] = flanking_region.to_i 
  end

  options[:path_sql] = File.join(File.dirname(__FILE__), "..", "database", "genomic_data.sqlite")
  opts.on("-p", "--path_sql PATH", "Path SQL DB to make queries") do |path|
    options[:path_sql] = path
  end

  options[:group] = nil
  opts.on("-g", '--group_by_region_type STRING', 'Use region type for group results by their coordinates') do |group|
  	options[:group] = group
  end

  options[:output_format] = 'html'
  opts.on("-f", '--output_format PATH', 'Output format for results. Default:html') do |output_format|
  	options[:output_format] = output_format
  end

  options[:output_path] = "results"
  opts.on("-o", '--output_path PATH', 'Output path for queries') do |output_path|
  	options[:output_path] = output_path
  end

  options[:representation] = FALSE
  opts.on("-r", '--graphical_representation', 'Make a representation of the selected region') do 
    options[:representation] = TRUE
  end

  options[:type] = []
  opts.on("-t", '--type_regions STRING', 'Region types to make the search. Format: region1,region2,region3...') do |type|
  	options[:type] = type.split(',')
  end

  options[:verbose] = nil
  opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
    options[:verbose] = v
  end

end.parse!

######################################################################################################################
## MAIN
######################################################################################################################
if !File.exists?(options[:path_sql])
  download_database(options[:path_sql])
end
$db = SQLite3::Database.new(options[:path_sql])
genomic_regions = []
#Benchmark.bm do |bm|
 # bm.report { 
    if !options[:coords].empty?
      genomic_regions = query_coordinates(options[:coords], options[:flanking_region])
    elsif !options[:name].empty?
      genomic_regions = query_name_and_region(options[:name], options[:flanking_region])
    end
 # }
#end
#puts 'FINAL', '---------------', genomic_regions.inspect
if !genomic_regions.empty?
  genomic_regions.select!{|reg| options[:name].include?(reg[5]) || reg[4] != options[:group]} if options[:input_names_only] && !options[:group].nil?
  simple_list(genomic_regions, options[:output_path], options[:output_format])
  simple_list(genomic_regions, options[:output_path], 'gff') if options[:representation]
  if !options[:group].nil?
    genomic_regions.concat(generate_query_regions(options[:coords])) if options[:group] == 'query_coords' && !options[:coords].empty?
    grouping_list(options[:group], genomic_regions, options[:output_path], options[:output_format])
  end
else
  puts 'Results not found'
end
