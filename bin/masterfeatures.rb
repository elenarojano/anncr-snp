#! /usr/bin/env ruby

require 'benchmark'

#Description
#--------------
#Tool to unify data from ENCODE

#Methods
#--------------

def load_metadata_file(file)
	name_storage = {}
	file_text = File.open(file)
	file_text.each do |line|
		line.chomp!
		fields = line.split("\t")
		features_storage = {} #metadata hash
		features = fields[1].split("; ")
		features.each do |feature|
			met_name, metadata = feature.split("=")
			features_storage[met_name] = metadata
		end
		name_storage[fields[0].gsub('.gz', '')] = features_storage
	end
	file_text.close
	return name_storage
end

def element_grouper(grouping_element, name_storage) #erase redundance by antibody by default
	package_grouping = {}
	name_storage.each do |file_name, metadata|
		selected_element = metadata[grouping_element]
		if !selected_element.nil?	 #verify is there is an element in this field (avoid mistakes).
			if !package_grouping[selected_element].nil?
				package_grouping[selected_element] << file_name
			else
				package_grouping[selected_element] = [file_name]
			end
		end
	end
	return package_grouping
end

def load_files_to_compare(file_name)
	genomic_regions = {}
	File.open(file_name).each do |line|
		line.chomp!
		genomic_info = line.split("\t")
		chr = genomic_info.shift
		genomic_info = genomic_info[0..1].map{|c| c.to_i}
		bin = genomic_info.first/10000
		query = genomic_regions[chr]
		if query.nil?
			genomic_regions[chr] = {bin => [genomic_info]}
		else
			query_bin = query[bin]
			if query_bin.nil?
				query[bin] = [genomic_info]
			else
				query_bin << genomic_info
			end
		end
	end
	return genomic_regions
end

def compare_genomics_regions(main_genomic_regions, genomic_regions_to_compare, thresold_overlap)
	selected_genomic_regions = {}
	genomic_regions_to_compare.each do |chr_reg, genomic_region_to_compare|
		genomic_region_to_compare.each do |bin, regs|
			batch_match = false
			query_main = main_genomic_regions[chr_reg] # main_genomic_regions has chr_reg?
			if !query_main.nil? # main_genomic_regions has chr_reg!
				query_main_bin = query_main[bin]
				if !query_main_bin.nil?
					batch_match = true
					regs.each do |reg|
						match = false
						query_main_bin.each do |main|
							match = compare_genomics_regions_coords(main, reg, thresold_overlap)
							break if match
						end
						save_reg(selected_genomic_regions, chr_reg, bin, reg) if !match
					end
				end
			end
			if !batch_match
				regs.each do |reg|
					save_reg(selected_genomic_regions, chr_reg, bin, reg)
				end
			end
		end
	end
	return selected_genomic_regions
end

def save_reg(selected_genomic_regions, chr_reg, bin, reg)
	query_chr = selected_genomic_regions[chr_reg]
	if !query_chr.nil?
		query_bin = query_chr[bin]
		if  query_bin.nil?
			query_chr[bin] = [reg]
		else
			query_bin << reg
		end
	else
		selected_genomic_regions[chr_reg] = {bin => [reg]}
	end
end

def save_reg_concat(selected_genomic_regions, chr_reg, bin, reg)
	query_chr = selected_genomic_regions[chr_reg]
	if !query_chr.nil?
		query_bin = query_chr[bin]
		if  query_bin.nil?
			query_chr[bin] = reg
		else
			query_bin.concat(reg)
		end
	else
		selected_genomic_regions[chr_reg] = {bin => reg}
	end
end

def compare_genomics_regions_coords(main_genomic_region, genomic_region_to_compare, thresold_overlap)
	match = false
	
	main_beg, main_end = main_genomic_region
	reg_beg, reg_end = genomic_region_to_compare
	size_main_genomic_region = main_end - main_beg
	size_genomic_region_to_compare = reg_end - reg_beg

	absolute_overlap = 0
	if reg_beg >= main_beg && reg_beg <= main_end
		absolute_overlap = main_end - reg_beg
	elsif reg_end >= main_beg && reg_end <= main_end
		absolute_overlap = reg_end - main_beg
	elsif reg_beg <= main_beg && reg_end >= main_end 
		absolute_overlap = size_main_genomic_region
	elsif reg_beg >= main_beg && reg_end <= main_end 
		absolute_overlap = size_genomic_region_to_compare
	end		
	main_relative_overlap = absolute_overlap / size_main_genomic_region * 1.0
	compare_relative_overlap = absolute_overlap / size_genomic_region_to_compare * 1.0 
	if main_relative_overlap >= thresold_overlap || compare_relative_overlap >= thresold_overlap
		match = true
	end

	return match
end

#Main
#--------------
file_input_folder = ARGV[3]
name_storage = load_metadata_file(ARGV[0])
package_grouping = element_grouper(ARGV[1], name_storage)
file_writer = File.open(ARGV[2],'w')

package_grouping.each do |grouping_element, file_names|
	#abrir el primer archivo del paquete
	genomic_regions_references = load_files_to_compare(File.join(file_input_folder, file_names.shift))
	file_names.each do |f_name|
		file2compare = load_files_to_compare(File.join(file_input_folder, f_name))
		selected_genomic_regions = selected_genomic_regions = compare_genomics_regions(genomic_regions_references, file2compare, 0.8)
		#puts Benchmark.measure{selected_genomic_regions = compare_genomics_regions(genomic_regions_references, file2compare, 0.8)}
		selected_genomic_regions.each do |chr, ge_regs|
			ge_regs.each do |bin, reg|
				save_reg_concat(genomic_regions_references, chr, bin, reg)
			end
		end		
	end
	genomic_regions_references.each do |chr, ge_regs|
		ge_regs.each do |bin, reg|
			reg.each do |gr|
				file_writer.puts "#{chr}\t#{gr.join("\t")}\t#{grouping_element}"
			end
		end
	end
	puts "Wrote #{grouping_element}"
end

file_writer.close





